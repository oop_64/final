/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thanakit.finalproject;

/**
 *
 * @author COOL
 */
public class inventory {
    private String code;
    private int item;
    private String price;
    private String balance;

    public inventory(String code, int item, String price, String balance) {
        this.code = code;
        this.item = item;
        this.price = price;
        this.balance = balance;
    }

    public String getcode() {
        return code;
    }

    public void setcode(String code) {
        this.code = code;
    }

    public int getitem() {
        return item;
    }

    public void setitem(int item) {
        this.item = item;
    }

    public String getprice() {
        return price;
    }

    public void setprice(String price) {
        this.price = price;
    }

    public String getbalance() {
        return balance;
    }

    public void setbalance(String balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "List{" + "code=" + code + ", item=" + item + ", price=" + price + ", balance=" + balance + '}';
    }
    
}

